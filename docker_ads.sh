#!/bin/bash

docker tag parser:0.1 caldeirada/ads10_parser
docker tag frontend:0.1 caldeirada/ads10_frontend
docker tag reasoner:0.1 caldeirada/ads10_reasoner

docker push caldeirada/ads10_parser
docker push caldeirada/ads10_frontend
docker push caldeirada/ads10_reasoner
