# Frontend

## How to run

1. Change directory to `cd frontend`
2. Update the submodules `git submodule update --init`
3. Change directory to `cd ./WebVOWL`
4. Run `yarn install`
5. Go back to the frontend folder `cd ..`
6. Run `yarn install`
7. Run `yarn start`
8. Open your browser at `localhost:8080`

## Development

The file `src/index.js` ends up being injected in the regular WebVOWL html, so use that file to add javascript to the WebVOWL implementation.

If absolutely necessary, the `src/index.html` file can be changed.

## Technologies

`React 17.0.1` JavaScript library for building user interfaces

`yarn 1.22.10` Package manager for the Javascript projects language

`webpack 5.9.0` Javascript module loader and bundler
