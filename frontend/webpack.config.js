const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const ENDPOINTS = {
  parser: `http://localhost:8083`,
};

module.exports = (env) => ({
  entry: './src/index.js',
  devtool: 'eval-source-map',
  devServer: {
    contentBase: './dist',
    hot: true,
    proxy: {
      '/convert': ENDPOINTS.parser,
    },
    writeToDisk: true,
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'extension.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [
            '@babel/transform-runtime',
            '@babel/plugin-proposal-class-properties',
          ],
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: './WebVOWL/deploy',
          filter: (resourcePath) => !resourcePath.includes('.html'),
          to: './',
        },
      ],
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      templateParameters: {
        version: process.env.CI_COMMIT_ID || 'unknown-version',
      },
    }),
  ],
});
