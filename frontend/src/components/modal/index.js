import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import './index.scss';

export class Modal extends PureComponent {
  state = { hidden: false };

  componentDidUpdate(prevProps) {
    if (prevProps.isLoading && !this.props.isLoading) {
      this.setState({ hidden: false });
    }
  }

  dismiss = () => {
    this.setState({ hidden: true });
    setTimeout(this.props.dismiss, 300);
  };

  render() {
    const { title, isLoading } = this.props;

    return (
      <div
        className={`modal-container ${this.state.hidden ? 'dismissed' : ''}`}
      >
        <div className='background' onClick={this.dismiss} />
        <div className='modal'>
          {title && <h1 className='title'>{title}</h1>}
          {this.props.children}
          {isLoading && <i className='loading'> loading ... </i>}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  isLoading: PropTypes.bool,
  title: PropTypes.string,
  dismiss: PropTypes.func.isRequired,
};
