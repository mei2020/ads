import React, { PureComponent } from 'react';

import { GuidedQuery } from '../guided-query';
import { Modal } from '../modal';
import { Query } from './query';
import { Results } from '../results';

import './index.scss';

const MOCK_ASK = {
  askResult: false,
};

const MOCK_SELECT = {
  parameters: ['batatas', 'cebolas'],
  selectResult: [
    ['Constraints', 'PreferenceIntegration'],
    ['ParticleUpdate', 'PreferenceIntegration'],
    ['LightBeamSearch', 'PreferenceModel'],
    ['ReferencePoint_based', 'PMOEA'],
    ['GeneralAdditive', 'UtilityFunction'],
    ['SetPreferenceRelation', 'PreferenceModel'],
    ['ReferenceDirection', 'PreferenceInformationFromDM'],
    ['PreferenceRegion', 'PreferenceModel'],
    ['SolutionComparison', 'PreferenceInformationFromDM'],
    ['BoundedRegion', 'SetOfSolutions'],
    ['GroupDecisionMaking', 'PreferenceInformationFromDM'],
    ['DiversityVSConvergence_based', 'MOEA'],
    ['SetPreference', 'PreferenceInformationFromDM'],
    ['PolyhedralConeBased', 'PreferenceModel'],
    ['WFG', 'Academic_Problem'],
    ['ResultInfluence', 'Thing'],
    ['ChoquetIntegral', 'UtilityFunction'],
    ['Linear', 'UtilityFunction'],
    ['QuasiConcave', 'UtilityFunction'],
    ['PreferenceRegion', 'PreferenceInformationFromDM'],
    ['Indicator', 'PreferenceInformationFromDM'],
    ['SelectionCriterion', 'PreferenceIntegration'],
    ['Probabilistic_based', 'MOEA'],
    ['OrdinalRegression', 'LearningMethod'],
    ['FrontSorting', 'PreferenceIntegration'],
    ['Knapsack', 'Academic_Problem'],
    ['UtilityFunction', 'PreferenceModel'],
    ['Memetic', 'MOEA'],
    ['Initalization', 'PreferenceIntegration'],
    ['BudgetofDMcalls', 'PreferenceInformationFromDM'],
    ['TerminationCriterion', 'PreferenceIntegration'],
    ['DecisionRules', 'PreferenceModel'],
    ['TerritorySize', 'PreferenceIntegration'],
    ['KneePoint', 'PreferenceModel'],
    ['SOEA', 'MetaHeuristic'],
    ['AchievementScalarizingFunction', 'PreferenceModel'],
    ['LinearProgramming', 'LearningMethod'],
    ['SampleSorts', 'SolutionComparison'],
    ['ObjectiveSpaceTransformation_basedPEMO', 'PMOEA'],
    ['QuadraticProgramming', 'LearningMethod'],
    ['AdditivePiecewiseLinear', 'UtilityFunction'],
    ['PreferenceInformationFromDM', 'Thing'],
    ['a-priori', 'InteractionTime'],
    ['Trade-off', 'PreferenceInformationFromDM'],
    ['PMOEA', 'MOEA'],
    ['PreferenceModel', 'Thing'],
    ['Academic_Problem', 'MOP'],
    ['OneSolution', 'ResultInfluence'],
    ['PSO_based', 'Swarm_based'],
    ['ASF', 'PreferenceIntegration'],
    ['DesirabilityFunction', 'UtilityFunction'],
    ['a-posteriori', 'InteractionTime'],
    ['Objectives', 'PreferenceIntegration'],
    ['ProgrammingLanguage', 'Thing'],
    ['SetOfSolutions', 'ResultInfluence'],
    ['DominanceRelation', 'PreferenceIntegration'],
    ['NeuralNetwork', 'LearningMethod'],
    ['MetaHeuristic', 'Thing'],
    ['Fitness', 'PreferenceIntegration'],
    ['SetQualityIndicator', 'PreferenceIntegration'],
    ['CrowdingDistance', 'PreferenceIntegration'],
    ['OutrankingRelation', 'PreferenceModel'],
    ['Researcher', 'Thing'],
    ['ObjectiveComparison', 'PreferenceInformationFromDM'],
    ['Decomposition_based', 'MOEA'],
    ['ReferencePoint', 'PreferenceInformationFromDM'],
    ['SupportVectorMachine', 'LearningMethod'],
    ['DTLZ', 'Academic_Problem'],
    ['Polynomial', 'UtilityFunction'],
    ['Ranking', 'PreferenceModel'],
    ['ACO_based', 'Probabilistic_based'],
    ['BiasedDistribution', 'SetOfSolutions'],
    ['LearningMethod', 'Thing'],
    ['Swarm_based', 'MOEA'],
    ['Coevolution_based', 'MOEA'],
    ['InteractionTime', 'Thing'],
    ['PreferenceIntegration', 'Thing'],
    ['progressive', 'InteractionTime'],
    ['DesirabilityFunction', 'PreferenceInformationFromDM'],
    ['Indicator_based', 'MOEA'],
    ['ImplementationLibrary', 'Thing'],
    ['ZDT', 'Academic_Problem'],
    ['FuzzyLogic', 'PreferenceModel'],
    ['MOP', 'Thing'],
    ['MOEA', 'MetaHeuristic'],
    ['WeightFunctionInObjectiveSpace', 'PreferenceRegion'],
    ['DistributionFunctionInObjectiveSpace', 'PreferenceRegion'],
    ['SampleRanks', 'SolutionComparison'],
    ['Realworld_Problem', 'MOP'],
    ['ObjectiveRelativeImportance', 'PreferenceModel'],
    ['OutrankingParameters', 'PreferenceInformationFromDM'],
    ['PreferencePoint', 'PreferenceModel'],
  ],
};

export class Form extends PureComponent {
  state = {
    response: null,
    submitted: false,
    ontology: {},
  };

  componentDidMount() {
    document.addEventListener('fileparsed', this.handleParsedFile);
  }

  handleParsedFile = (event) => {
    this.setState({ ontology: JSON.parse(event.detail) });
  };

  handleResponse = (response, query) => {
    console.log('got response', response);
    this.setState({ response, query });
  };

  handleSubmit = () => {
    this.setState({ submitted: true });
  };

  dismissModal = () => {
    this.setState({ submitted: false, response: null });
  };

  render() {
    const { response, submitted, ontology, query } = this.state;
    return (
      <div className='form'>
        <GuidedQuery
          ontology={ontology}
          onSubmit={this.handleSubmit}
          onResponse={this.handleResponse}
        />
        <Query onSubmit={this.handleSubmit} onResponse={this.handleResponse} />
        {submitted && (
          <Modal
            title='Query Results'
            isLoading={submitted && !response}
            response={response}
            dismiss={this.dismissModal}
          >
            <Results response={response} query={query} />
          </Modal>
        )}
      </div>
    );
  }
}
