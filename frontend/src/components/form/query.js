import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export class Query extends PureComponent {
  constructor() {
    super();
    this.state = { queryToDo: '' };
    this.changeQuery = this.changeQuery.bind(this);
  }

  changeQuery(e) {
    this.setState({ queryToDo: e.target.value });
  }

  async handleSubmit(query) {
    this.props.onSubmit();
    const finalQuery = encodeURIComponent(query);
    const response = await fetch(
      'http://localhost:8082/reasoner/sparql?query=' + finalQuery,
      {
        method: 'GET',
        responseType: 'application/json',
      }
    );
    const data = await response.json();
    this.props.onResponse(data, query);
  }

  render() {
    const { queryToDo } = this.state;
    return (
      <div className='query'>
        <h3>Advanced Query</h3>
        <textarea className='text-area' onKeyUp={this.changeQuery}></textarea>
        <button className='submit' onClick={() => this.handleSubmit(queryToDo)}>
          Submit Query
        </button>
      </div>
    );
  }
}

Query.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onResponse: PropTypes.func.isRequired,
};
