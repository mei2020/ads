import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import './index.scss';

const NEGATIVE_RESPONSE = 'Computer says NO!';
const POSITIVE_RESPONSE = 'Computer says yes!';

export class Results extends PureComponent {
  renderAskResult() {
    const { askResult } = this.props.response;
    return (
      <div className='ask-result'>
        {askResult ? <p>{POSITIVE_RESPONSE}</p> : <p>{NEGATIVE_RESPONSE}</p>}
      </div>
    );
  }

  renderTable() {
    const { selectResult, parameters } = this.props.response;
    return (
      <div className='table-container'>
        <table className='results-table'>
          <tr>
            {parameters.map((cell) => (
              <th>{cell}</th>
            ))}
          </tr>
          {selectResult.map((row) => (
            <tr>
              {row.map((cell) => (
                <td>{cell}</td>
              ))}
            </tr>
          ))}
        </table>
      </div>
    );
  }

  render() {
    const { response, query } = this.props;
    if (!response) {
      return null;
    }
    return (
      <Fragment>
        {response.askResult !== undefined && this.renderAskResult()}
        {response.selectResult && this.renderTable()}
        <h2>Query</h2>
        <code>{query}</code>
      </Fragment>
    );
  }
}

Results.propTypes = {
  response: PropTypes.shape({
    askResult: PropTypes.bool,
    parameters: PropTypes.arrayOf(PropTypes.string),
    selectResult: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  }),
};
