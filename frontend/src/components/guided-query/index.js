import React, { Component } from 'react';
import { parseToList } from '../../utils/JsonParser';

const QUERY_TYPE = {
  listAll: 'List all classes',
  askSubclassOf: 'Ask Direct Subclass',
  subclassPairs: 'Class/Subclass pairs',
  subclassOf: 'Subclass Of',
  type: 'Type',
}

export class GuidedQuery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleSelectClass: false,
      visibleSelectSecondClass: false,
      classe: '',
      classe2: '',
      type: QUERY_TYPE.listAll,
      classAttributes: [],
    };
    this.showSelectClass = this.showSelectClass.bind(this);
    this.changeClass = this.changeClass.bind(this);
    this.submitQuery = this.submitQuery.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.ontology === {} && this.props.ontology !== {}) ||
      this.props.ontology !== prevProps.ontology
    ) {
      this.setState({
        classAttributes: parseToList(this.props.ontology['classAttribute']),
      });
    }
  }

  showSelectClass(e) {
    if (e.target.value == QUERY_TYPE.subclassOf || e.target.value == QUERY_TYPE.type) {
      this.setState({
        visibleSelectClass: true,
        visibleSelectSecondClass: false,
        classe: this.state.classAttributes[0].value,
        type: e.target.value,
      });
    } else if (e.target.value == QUERY_TYPE.subclassPairs) {
      this.setState({
        visibleSelectClass: false,
        visibleSelectSecondClass: false,
        classe: '',
        type: QUERY_TYPE.subclassPairs,
      });
    } else if (e.target.value == QUERY_TYPE.askSubclassOf) {
      this.setState({
        visibleSelectClass: true,
        visibleSelectSecondClass: true,
        classe: this.state.classAttributes[0].value,
        classe2: this.state.classAttributes[0].value,
        type: e.target.value,
      });
    } else {
      this.setState({
        visibleSelectClass: false,
        visibleSelectSecondClass: false,
        classe: '',
        type: QUERY_TYPE.listAll,
      });
    }
  }

  changeClass(e) {
    this.setState({ visibleSelectClass: true, classe: e.target.value });
  }

  changeSecondClass(e) {
    this.setState({
      visibleSelectClass: true,
      visibleSelectSecondClass: true,
      classe2: e.target.value,
    });
  }

  async submitQuery() {
    this.props.onSubmit();
    let query = '';
    if (this.state.type == QUERY_TYPE.listAll)
      query = 'SELECT ?x WHERE {Class(?x)}';
    else if (this.state.type == QUERY_TYPE.type)
      query = 'SELECT * WHERE {Type(?x,' + this.state.classe + ')}';
    else if (this.state.type == QUERY_TYPE.subclassPairs)
      query = 'SELECT ?x ?b WHERE {DirectSubClassOf(?x, ?b)}';
    else if (this.state.type == QUERY_TYPE.subclassOf)
      query = 'SELECT * WHERE {DirectSubClassOf(?x,' + this.state.classe + ')}';
    else if (this.state.type == QUERY_TYPE.askSubclassOf)
      query =
        'ASK {DirectSubClassOf(' +
        this.state.classe +
        ',' +
        this.state.classe2 +
        ')}';
    const finalQuery = encodeURIComponent(query);
    const response = await fetch(
      'http://localhost:8082/reasoner/sparql?query=' + finalQuery,
      {
        method: 'GET',
        responseType: 'application/json',
      }
    );
    const data = await response.json();
    this.props.onResponse(data, query);
  }

  render() {
    const {
      visibleSelectClass,
      visibleSelectSecondClass,
      classAttributes,
    } = this.state;
    const options = [
      QUERY_TYPE.listAll,
      QUERY_TYPE.subclassPairs,
      QUERY_TYPE.subclassOf,
      QUERY_TYPE.type,
      QUERY_TYPE.askSubclassOf,
    ];
    console.log(classAttributes)
    return (
      <div className='query'>
        <header>
          Select the options in order to achieve the query you want
        </header>
        <div>
          <select className='dropdown-list' onChange={this.showSelectClass}>
            {options.map((option) => (
              <option className='dropdown-item' value={option}>
                {option}
              </option>
            ))}
          </select>
          {visibleSelectClass && (
            <select
              id='select_class'
              className='dropdown-list'
              onChange={this.changeClass}
            >
              {classAttributes.map((classe) => (
                <option className='dropdown-item' value={classe.value}>
                  {classe.name}
                </option>
              ))}
            </select>
          )}
          {visibleSelectSecondClass && (
            <select
              id='select_class'
              className='dropdown-list'
              onChange={this.changeSecondClass}
            >
              {classAttributes.map((classe) => (
                <option className='dropdown-item' value={classe.value}>
                  {classe.name}
                </option>
              ))}
            </select>
          )}
        </div>
        <button onClick={this.submitQuery}>Submit Query</button>
      </div>
    );
  }
}
