export function parseToList(content) {
  const classAttributes = [];
  for (var i = 0; i < content.length; i++) {
    if (content[i].label !== undefined) {
      if (content[i].label["IRI-based"] === undefined) {
        if (!contains(classAttributes, content[i].label["undefined"])) {
          classAttributes.push({ name: content[i].label["undefined"], value: '<' + content[i].iri + '>' });
        }
      }
      else if (content[i].label["en"] === undefined) {
        if (!contains(classAttributes, content[i].label["IRI-based"])) {
          classAttributes.push({ name: content[i].label["IRI-based"], value: '<' + content[i].iri + '>' });
        }
      }
      else {
        if (!contains(classAttributes, content[i].label["en"])) {
          classAttributes.push({ name: content[i].label["en"], value: '<' + content[i].iri + '>' });
        }
      }
    }
  }
  return classAttributes;
}

function contains(classAttributes, nameToCheck) {
  for (var i = 0; i < classAttributes.length; i++) {
    if (classAttributes[i].name == nameToCheck)
      return true;
  }
  return false;
}