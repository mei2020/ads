import React from 'react';
import ReactDOM from 'react-dom';

import { Form } from './components/form';

import './css/index.scss';

console.log('extension loaded');

ReactDOM.render(
  <React.StrictMode>
    <Form />
  </React.StrictMode>,
  document.getElementById('generalDetailsQuery')
);
