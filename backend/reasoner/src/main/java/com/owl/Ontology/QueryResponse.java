package com.owl.Ontology;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueryResponse {

    private List<String> parameters;
    private List<List<String>> selectResult;
    private Boolean askResult = null;

    public List<List<String>> getSelectResult() {
        return selectResult;
    }

    public void setSelectResult(List<List<String>> selectResult) {
        this.selectResult = selectResult;
    }

    public Boolean isAskResult() {
        return askResult;
    }

    public void setAskResult(boolean askResult) {
        this.askResult = askResult;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }
}
