package com.owl.Ontology.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ReasonerOwlCreationException extends RuntimeException {

    public ReasonerOwlCreationException() {

    }

    public ReasonerOwlCreationException(Throwable cause) {
        super(cause);
    }
}
