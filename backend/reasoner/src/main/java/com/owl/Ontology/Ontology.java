package com.owl.Ontology;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.owl.Ontology.Exception.ReasonerOwlCreationException;
import com.owl.Ontology.Exception.ReasonerOwlQueryError;
import de.derivo.sparqldlapi.Query;
import de.derivo.sparqldlapi.QueryArgument;
import de.derivo.sparqldlapi.QueryEngine;
import de.derivo.sparqldlapi.QueryResult;
import de.derivo.sparqldlapi.exceptions.QueryEngineException;
import de.derivo.sparqldlapi.exceptions.QueryParserException;
import de.derivo.sparqldlapi.types.QueryType;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Ontology {

    Logger logger = LoggerFactory.getLogger(Ontology.class);

    private final String LABEL_SPLIT = "#";

    private OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    private QueryEngine engine;
    private OWLOntology ontology;

    public Ontology(InputStream inputStream) {
        try {
            this.ontology = manager.loadOntologyFromOntologyDocument(inputStream);

            StructuralReasonerFactory factory = new StructuralReasonerFactory();
            OWLReasoner reasoner = factory.createReasoner(ontology);
            reasoner.precomputeInferences(InferenceType.CLASS_ASSERTIONS, InferenceType.OBJECT_PROPERTY_ASSERTIONS);
            engine = QueryEngine.create(manager, reasoner, true);

        } catch (OWLOntologyCreationException e) {
            logger.error("Error while handling owl file", e);
            throw new ReasonerOwlCreationException(e);
        }
    }

    public QueryResponse queryOntologySparql(String query) {
        try {
            Query q = Query.create(query);
            QueryResult result = engine.execute(q);

            QueryResponse response = new QueryResponse();

            if (q.getType().equals(QueryType.ASK)){
                response.setAskResult(result.ask());
            } else {
                response.setParameters(q.getResultVars().stream().map(QueryArgument::getValueAsString).collect(Collectors.toList()));
                response.setSelectResult(getLabels(result, response.getParameters()));
            }

            return response;
        } catch (QueryParserException | QueryEngineException e) {
            logger.error("Error while handling owl file", e);
            throw new ReasonerOwlQueryError(e);
        }
    }

    private List<List<String>> getLabels(QueryResult result, List<String> vars) {
        Gson gson = new Gson();
        JsonElement json = gson.fromJson(result.toJSON(), JsonElement.class);

        JsonElement results = ((JsonObject) json).get("results");

        JsonElement bindings = ((JsonObject) results).get("bindings");

        List<List<String>> labels = new ArrayList<>();
        for (int i = 0;i < ((JsonArray) bindings).size() ;i ++) {
            JsonElement next = ((JsonArray) bindings).get(i);
            List<String> rows = new ArrayList<>();
            for (String var : vars) {
                JsonElement values = ((JsonObject) next).get(var);
                rows.add(getLabel(((JsonObject) values).get("value").getAsString()));
            }
            labels.add(rows);
        }
        return labels;
    }

    private String getLabel(String ref) {
        for (OWLAnnotationAssertionAxiom axiom : ontology.getAnnotationAssertionAxioms(IRI.create(ref))) {
            if (axiom.getProperty().isLabel()) {
                if (axiom.getValue() instanceof OWLLiteral) {
                    OWLLiteral val = (OWLLiteral) axiom.getValue();
                    return val.getLiteral();
                }
            }
        }

        if (ref.contains(LABEL_SPLIT)) {
            return ref.split(LABEL_SPLIT)[1];
        }
        return ref;
    }
}
