package com.owl.Ontology.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ReasonerOwlQueryError extends RuntimeException {

    public ReasonerOwlQueryError() {

    }

    public ReasonerOwlQueryError(Throwable cause) {
        super(cause);
    }
}
