package com.owl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationReasoner {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationReasoner.class, args);
    }

}
