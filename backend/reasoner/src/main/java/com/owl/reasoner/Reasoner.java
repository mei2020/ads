package com.owl.reasoner;

import com.owl.Ontology.Ontology;
import com.owl.Ontology.QueryResponse;
import org.springframework.web.multipart.MultipartFile;

public interface Reasoner {
    Ontology getOwl();

    QueryResponse queryOwlSparql(String query);

    void setOwl(MultipartFile owlFile);
}
