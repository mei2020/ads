package com.owl.reasoner;

import java.io.*;

import com.owl.Ontology.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.owl.Ontology.Ontology;


@RestController
public class ReasonerController {

    @Autowired
    private Reasoner reasoner;

    public ReasonerController(Reasoner reasoner) {
        this.reasoner = reasoner;
    }

    @GetMapping("/reasoner/sparql")
    @CrossOrigin
    public QueryResponse queryOntologySparql(@RequestParam("query") String query) {
        return reasoner.queryOwlSparql(query);
    }

    @PostMapping("/reasoner")
    public void setOwlFile(@RequestParam("ontology") MultipartFile file) throws IOException {
        reasoner.setOwl(file);
    }

	public Ontology getOntology() {
		return reasoner.getOwl();
	}
}
