package com.owl.reasoner;

import java.io.*;

import com.owl.Ontology.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.owl.Ontology.Ontology;
import com.owl.Ontology.Exception.ReasonerOwlCreationException;

import javax.annotation.PostConstruct;

@Service
public class ReasonerImpl implements Reasoner{

    Logger logger = LoggerFactory.getLogger(Reasoner.class);

    private Ontology owl;

    @PostConstruct
    public void startUp() throws FileNotFoundException {
        InputStream file = new FileInputStream(System.getProperty("user.dir")+"/resources/PMOEA.owl");
        owl = new Ontology(file);
    }

    @Override
    public Ontology getOwl() {
        return owl;
    }

    @Override
    public QueryResponse queryOwlSparql(String query) {
        return owl.queryOntologySparql(query);
    }

    @Override
    public void setOwl(MultipartFile owlFile) {
        try {
            InputStream inputStream =  new BufferedInputStream(owlFile.getInputStream());

            owl = new Ontology(inputStream);
        } catch (IOException  e) {
            logger.error("Error parsing input file", e);
            throw new ReasonerOwlCreationException(e);
        }

    }
}
