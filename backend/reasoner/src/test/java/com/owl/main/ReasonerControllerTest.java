package com.owl.main;

import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.owl.ApplicationReasoner;
import com.owl.reasoner.ReasonerController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {ApplicationReasoner.class ,ReasonerController.class})
public class ReasonerControllerTest {
	
	@Autowired
	private ReasonerController controller;
	
	@Mock
	MultipartFile file;

	@Test
	public void test() throws IOException {
		assertNotNull(controller);
		InputStream in = new FileInputStream(System.getProperty("user.dir")+"/src/test/java/PMOEA.owl");
		file = new MockMultipartFile("name",in);
		controller.setOwlFile(file);
		assertNotNull(controller.getOntology());
		assertNotNull(controller.queryOntologySparql("PREFIX PMOEA: <http://www.semanticweb.org/longmeili/ontologies/PMOEA#>\r\n" + 
				"SELECT *\r\n" + 
				"WHERE { Type(?x, PMOEA:OWLClass_b70f801e_7369_45e9_84a9_6b76db7c4c06)}"));
	}

}
