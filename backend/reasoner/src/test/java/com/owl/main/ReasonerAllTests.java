package com.owl.main;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ReasonerControllerTest.class, OntologyTest.class, ReasonerTest.class })
public class ReasonerAllTests {

}
