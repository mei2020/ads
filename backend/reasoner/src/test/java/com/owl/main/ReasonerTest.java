package com.owl.main;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.owl.Ontology.Exception.ReasonerOwlCreationException;
import com.owl.Ontology.Exception.ReasonerOwlQueryError;
import com.owl.reasoner.Reasoner;
import com.owl.reasoner.ReasonerImpl;

public class ReasonerTest {

	MultipartFile file;

	@Test
	public void test() throws IOException {
		Reasoner r = new ReasonerImpl();
		Executable ex = new Executable() {
			
			@Override
			public void execute() throws Throwable {
				MultipartFile file1 = new MockMultipartFile("asd", new FileInputStream(""));
				r.setOwl(file1);
			}
		};
		assertThrows(FileNotFoundException.class, ex);
		InputStream in = new FileInputStream(System.getProperty("user.dir")+"/src/test/java/PMOEA.owl");
		file = new MockMultipartFile("name",in);
		
		r.setOwl(file);
		assertNotNull(r.queryOwlSparql("PREFIX PMOEA: <http://www.semanticweb.org/longmeili/ontologies/PMOEA#>\r\n" + 
				"SELECT *\r\n" + 
				"WHERE { Type(?x, PMOEA:OWLClass_b70f801e_7369_45e9_84a9_6b76db7c4c06)}"));
		assertNotNull(r.getOwl());
		ReasonerOwlCreationException e = new ReasonerOwlCreationException();
		assertNotNull(e);
		ReasonerOwlQueryError error = new ReasonerOwlQueryError();
		assertNotNull(error);
	}

}
