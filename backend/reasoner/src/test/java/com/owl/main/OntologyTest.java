package com.owl.main;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import de.derivo.sparqldlapi.exceptions.QueryParserException;
import org.junit.Test;
import org.junit.jupiter.api.function.Executable;

import com.owl.Ontology.Ontology;
import com.owl.Ontology.Exception.ReasonerOwlQueryError;

public class OntologyTest {

	private Ontology ont = null;
	
	
    @Test
	public void test() {
		try {
			ont = new Ontology(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/PMOEA.owl"));
			assertNotNull(ont);
			assertNotNull(ont.queryOntologySparql("SELECT ?batatas ?cebolas WHERE {DirectSubClassOf(?batatas,?cebolas)}"));
			Executable ex = () -> {
				// TODO Auto-generated method stub
				ont = new Ontology(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/PMOEA.owl"));
				ont.queryOntologySparql("SELECT ?batatas ?cebolas WHERES {DirectSubClassOf(?batatas,?cebolas)}");

			};
			assertThrows(ReasonerOwlQueryError.class, ex );
		} catch (FileNotFoundException e) {}
		
	}

}
