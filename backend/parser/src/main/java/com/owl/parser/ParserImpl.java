package com.owl.parser;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import de.uni_stuttgart.vis.vowl.owl2vowl.Owl2Vowl;

@Service
public class ParserImpl implements Parser{
	
	public String owlToJSon(MultipartFile multfile)  {
		try(InputStream in = multfile.getInputStream()){
			Owl2Vowl owl2vowl = new Owl2Vowl(in);
			return owl2vowl.getJsonAsString();
		} catch (IOException e) {
			return null;
		}
	}
	
}