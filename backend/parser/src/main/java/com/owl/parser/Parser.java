package com.owl.parser;

import org.springframework.web.multipart.MultipartFile;

public interface Parser {	

	public String owlToJSon(MultipartFile path);
}
