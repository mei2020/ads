package com.owl.parser;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ParserController {

	@Autowired
	private Parser parser;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("/convert")
	public String parseToJson(@RequestParam("ontology") MultipartFile file) throws IOException{
		return parser.owlToJSon(file);
	}

}
