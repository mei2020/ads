package com.owl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationParser {

	 public static void main(String[] args) {
	        SpringApplication.run(ApplicationParser.class, args);
	    }
}
