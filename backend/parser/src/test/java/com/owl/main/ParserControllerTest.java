package com.owl.main;

import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.owl.ApplicationParser;
import com.owl.parser.ParserController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {ApplicationParser.class ,ParserController.class})
public class ParserControllerTest {
	
	@Autowired
	private ParserController p;
	
	@Mock
	MultipartFile file;

	
	@Test
	public void test() throws IOException{
		assertNotNull(p);
		InputStream in = new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/owl/main/PMOEA.owl");
		file = new MockMultipartFile("name",in);
		assertNotNull(p.index());
		assertNotNull(p.parseToJson(file));
	}

}
