package com.owl.main;

import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.owl.parser.Parser;
import com.owl.parser.ParserImpl;

public class ParserTest {
	
	Parser p;
	MultipartFile fileNotNull;
	
	@Before
	public void before() throws IOException {
		p = new ParserImpl();
		InputStream in = new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/owl/main/PMOEA.owl");
		fileNotNull = new MockMultipartFile("name", in);
	}


	@Test
	public void test() throws IOException {
		assertNotNull(p);
		assertNotNull(p.owlToJSon(fileNotNull));
	}

}
