FROM node:10.15.3-alpine AS builder

COPY ./.gitmodules ./.gitmodules
COPY ./frontend ./frontend
COPY ./.git ./.git

WORKDIR /frontend

RUN apk add git
RUN npm install
RUN yarn init:modules
RUN yarn build

FROM nginx:stable-alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder frontend/dist/ /usr/share/nginx/html
COPY frontend/nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]